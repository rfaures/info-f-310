from datetime import datetime
from random import *
from Models.Chromosome import *
import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
import math


def nsga_ii(size_pop, mutation_rate=0.25, decrease_mut=False, max_gen=None, max_time=None):

    # Initiating stop conditions
    gen = 0
    start = datetime.now()
    gen_wo_improvement = 0
    previous_front = []

    # Decreasing mutation rate
    if max_gen and decrease_mut:
        if decrease_mut == "lin":
            mutation_rates = np.linspace(mutation_rate, 0, max_gen)
        elif decrease_mut == "log":
            mutation_rates = np.logspace(np.log10(mutation_rate), -3, max_gen)

    pop = initialize_pop(size_pop)

    fronts = non_dom_sort(pop)
    calculate_crowd_dist(fronts)

    while 1:

        next_pop = []

        if max_gen and decrease_mut:
            mutation_rate = mutation_rates[gen]

        # Creating new generation
        children = produce_offspring(pop, mutation_rate)
        pop.extend(children)

        # For all but first front, apply mutations
        for front in fronts[1:]:
            for ind in front:
                mutation(ind, mutation_rate)
        for ind in children:
            mutation(ind, mutation_rate)

        # Delete duplicates
        pop = list(set(pop))

        fronts = non_dom_sort(pop)
        calculate_crowd_dist(fronts)

        # Shrink population to size_pop
        for front in fronts:
            if len(next_pop) + len(front) <= size_pop:
                next_pop.extend(front)
            else:
                next_pop.extend(front[:size_pop - len(next_pop)])
                break

        pop = next_pop

        gen += 1

        # Stop condition
        if max_time and (datetime.now() - start).seconds > max_time:
            break
        if max_gen and gen >= max_gen:
            break

        # Did front 0 make progress?
        b = True
        if gen > 0:
            new_elements = []
            for chr in set(fronts[0]):
                if chr not in previous_front:
                    previous_front = set(fronts[0])
                    new_elements.append(chr)
                    print(previous_front)
                    gen_wo_improvement = 0
                    b = False

        if b:
            gen_wo_improvement += 1
            print(gen_wo_improvement)

        if gen_wo_improvement > 200:
            print("Stopped at gen: " + str(gen))
            if invert(fronts[0]):
                break
            print(set(fronts[0]))
            gen_wo_improvement = 0

    return fronts[0], gen


def initialize_pop(size_pop):
    """
    Population initialisation

    Returns an array of size_pop random chromosomes to populate the first generation.

    :param size_pop: number of chromosomes generated
    :return: array of random chromosomes
    """
    pop = list()
    while len(pop) < size_pop:
        pop.append(random_chromosome())

    return pop


def random_chromosome():
    """
    Chromosome initialisation

    Returns a random chromosome.

    It first creates a random array of the 19 districts then splits it in three parts
    of length defined by the partition for the three trucks.

    :return: random chromosome
    """
    districts = list(range(0, 19))
    itinerary = list()

    while len(districts) > 0:
        itinerary.append(districts.pop(randint(0, len(districts) - 1)))

    # I give two times more chances of selecting sizes (6,6,7)
    partitions = [(5, 10), (5, 11), (5, 12), (6, 12), (7, 13)]
    s1, s2 = partitions[randrange(4)]
    chromosome = Chromosome(itinerary[:s1], itinerary[s1:s2], itinerary[s2:])

    return chromosome


def non_dom_sort(pop):
    """
    Sorts according to the non-dominated sorting method of the NSGA II.
    For each individual, this method sets an attribute front according to the front it belongs to.

    :param pop: the population to sort
    :return: an array of the different fronts, sorted from 0 to last.
    """
    fronts = [[]]
    ranked_count = 0

    for ind1 in range(len(pop)):
        dominated = []
        dominate_count = 0

        for ind2 in range(len(pop)):
            if (pop[ind1].get_distance() <= pop[ind2].get_distance() and pop[ind1].get_risk() <= pop[ind2].get_risk()) and (
                    pop[ind1].get_distance() < pop[ind2].get_distance() or pop[ind1].get_risk() < pop[ind2].get_risk()):
                dominated.append(pop[ind2])
            elif (pop[ind1].get_distance() >= pop[ind2].get_distance() and pop[ind1].get_risk() >= pop[ind2].get_risk()) and (
                    pop[ind1].get_distance() > pop[ind2].get_distance() or pop[ind1].get_risk() > pop[ind2].get_risk()):
                dominate_count += 1

        pop[ind1].dominate_count = dominate_count
        pop[ind1].dominated = dominated

        if dominate_count == 0:
            pop[ind1].front = 0
            fronts[0].append(pop[ind1])
            ranked_count += 1

    front = 0
    while ranked_count != len(pop):
        fronts.append([])
        for ind in fronts[front]:
            for dominated in ind.dominated:
                dominated.dominate_count -= 1
                if dominated.dominate_count == 0:
                    dominated.front = front + 1
                    fronts[front + 1].append(dominated)
                    ranked_count += 1
        front += 1

    return fronts


def calculate_crowd_dist(fronts):
    """
        Calculate and set the crowding distance of each individual for every front of fronts.

        :param fronts: array of fronts of chromosomes.
    """

    for front in fronts:
        # o = {0, 1} -> {dist, risk}
        for obj in range(2):

            front.sort(key=lambda elem: elem.get_objective(obj))

            front[0].crowd_dist = math.inf
            front[-1].crowd_dist = math.inf
            for i in range(1, len(front) - 1):
                if front[-1].get_objective(obj) == front[0].get_objective(obj):
                    front[i].crowd_dist = math.inf
                else:
                    front[i].crowd_dist += (front[i + 1].get_objective(obj) - front[i - 1].get_objective(obj)) / (
                            front[-1].get_objective(obj) - front[0].get_objective(obj))

        front.sort(key=lambda elem: elem.crowd_dist, reverse=True)


def produce_offspring(pop):
    """
    Select parents and execute crossover to create children.

    :param pop: population of parents
    :return: array of children
    """
    children = []

    while len(children) < len(pop):
        parent_1 = tournament_selection(pop)
        parent_2 = tournament_selection(pop)

        child_1, child_2 = crossover(parent_1, parent_2)

        children.append(child_1)
        children.append(child_2)

    return children


def tournament_selection(pop):
    """
    Randomly selects two individuals and make them compete to be a parent.

    :param pop: population of parents
    :return: individual that won
    """
    index_1 = randrange(len(pop))
    index_2 = randrange(len(pop))
    candidate_1 = pop[index_1]
    candidate_2 = pop[index_2]

    winner = compare_candidates(candidate_1, candidate_2)

    return winner


def compare_candidates(candidate_1, candidate_2):
    """
    Compare two individuals based on front, then crowding distance.

    :param candidate_1:
    :param candidate_2:
    :return: the best individual
    """
    if candidate_1.front < candidate_2.front:
        return candidate_1
    elif candidate_1.front == candidate_2.front:
        if candidate_1.crowd_dist > candidate_1.crowd_dist:
            return candidate_1
        else:
            return candidate_2
    else:
        return candidate_2


def crossover(parent_1, parent_2):
    """
    OX cross-over from two parents that produce two children.

    :param parent_1:
    :param parent_2:
    :return: the two children produced
    """
    parent_1_DNA = parent_1.get_DNA()
    parent_2_DNA = parent_2.get_DNA()

    child_1_DNA = []
    child_2_DNA = []

    i = randrange(19)
    j = randrange(18)

    start = min(i, j)
    end = max(i, j)

    child_1_DNA[start:end] = parent_1_DNA[start:end]
    child_2_DNA[start:end] = parent_2_DNA[start:end]

    for k in range(19):
        index = (end + k) % 19

        id_1 = parent_1_DNA[index]
        id_2 = parent_2_DNA[index]

        if id_2 not in child_1_DNA:
            child_1_DNA.append(id_2)

        if id_1 not in child_2_DNA:
            child_2_DNA.append(id_1)

    child_1_DNA = rotate(child_1_DNA, start)
    child_2_DNA = rotate(child_2_DNA, start)

    child_1 = Chromosome(parent_1.get_itinerary()[0], parent_1.get_itinerary()[1], parent_1.get_itinerary()[2])
    child_1.set_DNA(child_1_DNA)
    child_2 = Chromosome(parent_2.get_itinerary()[0], parent_2.get_itinerary()[1], parent_2.get_itinerary()[2])
    child_2.set_DNA(child_2_DNA)

    return child_1, child_2


def rotate(ls, n):
    return ls[-n:] + ls[:-n]


def mutation(individual, mutation_rate):
    """
    Performs the mutation of an individual by swapping neighbours according to mutation_rate.
    This permutation allows for wrap-arounds. To exclude those cases, use
    index1 = randrange(len(DNA) - 1) ; index2 = (index1 + 1) instead.

    :param individual: the individual to mutate
    :param mutation_rate: 0.01 = 1 permutation, 1 = 100 permutations
    :return: the individual after mutation
    """

    DNA = individual.get_DNA()
    # print(DNA)

    if random() < mutation_rate:
        i = randrange(len(DNA))
        j = randrange(len(DNA))
        DNA[i], DNA[j] = DNA[j], DNA[i]
        individual.set_DNA(DNA)


def invert(front):
    """
    Tries inverting trucks itinerary to decrease risk while conserving distance.

    :param front: the front of individuals on which inverting is tested
    :return: True if inverting has not decreased risk, False otherwise
    """
    not_changed = True
    for ind in front:
        for i in range(len(ind.trucks)):
            rev_itinerary = []
            for stop in ind.trucks[i].itinerary:
                rev_itinerary.insert(0, stop.id)
            rev_itinerary = Itinerary(rev_itinerary)
            if rev_itinerary.evaluate_risk() < ind.trucks[i].evaluate_risk():
                ind.trucks[i] = rev_itinerary
                ind.evaluate_distance()
                ind.evaluate_risk()
                not_changed = False

    return not_changed


def rand_pop(size_pop, max_gen=None, max_time=None):
    """
    Random model used as benchmark.

    :param size_pop: maximum size of population
    :param max_gen: maximum number of generations
    :param max_time: maximum computing time
    :return: final fronts
    """
    pop = initialize_pop(size_pop)
    start = datetime.now()
    gen = 0
    previous_front = None
    gen_wo_improvement = 0
    while 1:
        fronts = non_dom_sort(pop)
        pop = initialize_pop(size_pop)
        pop += fronts[0]
        if max_time and (datetime.now() - start).seconds > max_time:
            break
        if max_gen and gen > max_gen:
            break
        gen += 1
        if set(fronts[0]) == previous_front:
            gen_wo_improvement += 1
        else:
            previous_front = set(fronts[0])
            gen_wo_improvement = 0
        if gen_wo_improvement <= -1:
            break

    return fronts


# -#-#---- Displaying results ----#-#- #

def display_population(fronts, n, dir=None):
    """
         Display the solutions of the iteration n in a graph
         The four first front are in a specific color, the others are numbered

         :param fronts: population array of chromosomes, sort by front
         :param n : number of the population generation
         :param dir: directory in which the plot is saved
    """
    plt.clf()
    plt.title("Population n°"+str(n))
    for i in range(len(fronts)):
        for j in range(len(fronts[i])):
            x = fronts[i][j].get_distance()
            y = fronts[i][j].get_risk()
            if (i == 0):
                plt.plot(x, y, 'ro', markersize=5)
                plt.text(x + 0.1, y, i, fontsize=7)
            elif (i == 1):
                plt.plot(x, y, 'bo', markersize=5)
                plt.text(x + 0.1, y, i, fontsize=7)
            elif (i == 2):
                plt.plot(x, y, 'go', markersize=5)
                plt.text(x + 0.1, y, i, fontsize=7)
            elif (i == 3):
                plt.plot(x, y, 'yo', markersize=5)
                plt.text(x + 0.1, y, i, fontsize=7)
            else:
                plt.plot(x, y, 'ko', markersize=5)
                plt.text(x + 0.1, y, i, fontsize=7)
    plt.ylabel('Risque')
    plt.xlabel('Distance')
    if dir is not None:
        plt.savefig("plots/"+dir+"/fig"+ str(n))
    else:
        plt.show()


def extract_best(front):
    """
    Finds the individual with the lowest distance and the one with the lowest risk.

    :param front:
    :return:
    """
    best_dist = front[0]
    best_risk = front[0]
    for chr in front[1:]:
        if best_dist.get_distance() > chr.get_distance():
            best_dist = chr
        if best_risk.get_risk() > chr.get_risk():
            best_risk = chr
    print("Best Distance ("+str(best_dist.get_distance())+") :")
    print(best_dist.get_itinerary())
    print("Best Risk (" + str(best_risk.get_risk()) + ") :")
    print(best_risk.get_itinerary())


def compare_models(models_front0):
    """
    Plots the scatter plot of the best front of each model, then plots a bor plot of the density
    of the different models: the proportion of individuals from a given model that are in the front X
    when a non dominated sorting is performed on the population composed of the best front of each model.

    :param models_front0: the best front of each model tested
    """
    display_population(models_front0, "Best")

    all_chromosomes = []
    for model in models_front0:
        all_chromosomes += model

    new_fronts = non_dom_sort(all_chromosomes)

    models_fronts = []

    for model in models_front0:
        model_fronts = []
        front_num = 0
        for front in new_fronts:
            for chr in model:
                if chr in front:
                    model_fronts.append(front_num)
            front_num += 1
        models_fronts.append(model_fronts)

    extract_best(new_fronts[0])

    models_front_dist = []
    for model_fronts in models_fronts:
        front_dist = []
        for i in range(len(new_fronts)):
            count = 0
            for front in model_fronts:
                if i == front:
                    count += 1
            count /= len(model_fronts)
            front_dist.append(count)
        models_front_dist.append(front_dist)

    plt.style.use("seaborn")
    i = 0
    w = 0.8 / len(models_front_dist)
    for front_dist in models_front_dist:
        plt.bar(np.array(range(len(new_fronts))) + (i * w), front_dist, w)
        i += 1

    plt.xticks(np.array(range(len(new_fronts))) + (w * (len(models_front_dist) - 1) / 2), range(len(new_fronts)))
    plt.title("Models distribution on fronts")
    plt.xlabel("Front number")
    plt.ylabel("Density")
    plt.savefig('Barplots/'+str(datetime.now())+'.png')
    plt.show()


def randomized_search(max_pop_size, max_mut_range, iterations=100):
    """
    Performs a comparison of randomly selected features sets.
    Results are stored in excel and csv files.

    :param max_pop_size: maximum size of population tested
    :param max_mut_range: maximum mutation rate tested
    :param iterations: number of features sets tested
    """
    step = 0.001
    grid = []

    # Making the grid
    for p in range(10, max_pop_size + 1):
        ls1 = []
        for m in np.arange(0, max_mut_range, step):
            ls2 = []
            for md in [False, "lin", "log"]:
                ls2.append((p, m, md))
            ls1.append(ls2)
        grid.append(ls1)

    for j in range(10):
        front0_tested = []
        features_set_tested = []
        for i in range(iterations):
            features_set = grid[randrange(max_pop_size - 10)][randrange(max_mut_range / step)][randrange(3)]
            print("Testing feature set :", end=" ")
            print(features_set)
            features_set_tested.append(features_set)
            front0_tested.append(nsga_ii(features_set[0], mutation_rate=features_set[1], decrease_mut=features_set[2], max_gen=300))

        print(front0_tested)
        compare_models_randomized(front0_tested, features_set_tested, j)


def compare_models_randomized(models_front0, features_tested, j):

    all_chromosomes = []
    for model in models_front0:
        all_chromosomes += model[0] # model[1] being the number of generations until killed

    new_fronts = non_dom_sort(all_chromosomes)

    best = 0
    index = 0
    dic = {"pop_size":[], "max_mut_rate":[], "decreasing_mut":[], "chr_in_front":[], "gen_before_kill":[]}
    for i in range(len(new_fronts)):
        dic["front "+str(i)] = []

    print(dic)

    models_front_dist = []
    for model in models_front0:
        dic["pop_size"].append(features_tested[index][0])
        dic["max_mut_rate"].append(features_tested[index][1])
        dic["decreasing_mut"].append(features_tested[index][2])
        dic["chr_in_front"].append(len(model[0]))
        dic["gen_before_kill"].append(model[1])
        front_dist = []
        for front in range(len(new_fronts)):
            chr_in_front = 0
            for chr in model[0]:
                if chr in new_fronts[front]:
                    chr_in_front += 1
            front_dist.append(chr_in_front/len(model[0]))
            dic["front "+str(front)].append(chr_in_front/len(model[0]))
        models_front_dist.append(front_dist)
        if front_dist[0] > best:
            best = front_dist[0]
            best_index = index
        index += 1

    df = pd.DataFrame(dic)
    df = df.sort_values(["front "+str(i) for i in range(len(new_fronts))], ascending=False)
    print(df)

    df.to_csv('random_search'+str(j)+'.csv')
    df.to_excel('random_search'+str(j)+'.xlsx')

    print("Best features are:", end=" ")
    print(features_tested[best_index])


def retest_best_from_csv():
    """
    Takes the best features
    :return:
    """
    front0_tested = []
    features_set_tested = []
    for i in range(10):
        df = pd.read_csv("random_search"+str(i)+".csv")
        it = df.iterrows()
        j = 0
        while j < 3:
            j, row = next(it)
            features_set = [row["pop_size"], row["max_mut_rate"], row["decreasing_mut"]]
            if features_set[2] == 'False':
                features_set[2] = False
            print("Testing feature set :", end=" ")
            print(features_set)
            features_set_tested.append(features_set)
            front0_tested.append(
                nsga_ii(features_set[0], mutation_rate=features_set[1], decrease_mut=features_set[2], max_gen=300))
    compare_models_randomized(front0_tested, features_set_tested, "best")


def display_progression(best_front_iterations, n):
    """
    Plots the evolution of the solutions during the successive generations

    :param best_front_iterations:
    :param n:
    """
    from colour import Color

    plt.clf()
    plt.title("Population n°" + str(n))

    red = Color("red")
    colors = list(red.range_to(Color("green"), len(best_front_iterations)))

    index = 0
    for front in best_front_iterations:
        col = (round(colors[index].get_red(), 3), round(colors[index].get_green(), 3), round(colors[index].get_blue(), 3))
        if index == len(best_front_iterations)-1:
            col = 'black'
        print(col)
        for chrom in front:
            if chrom.check_constraints():
                x = chrom.get_distance()
                y = chrom.get_risk()

                plt.scatter(x, y, c=col, s=10)

        index += 1

    plt.ylabel('Risque')
    plt.xlabel('Distance')

    plt.show()


def solutions_to_csv(front):
    """
    Saves the best solutions to csv format required by the teacher.

    :param front:
    """
    with open("solutions/solutions.csv", 'a') as solutions_csv:
        for elem in front:
            solutions_csv.write("0," + ",".join(str(e+1) for e in elem.get_DNA()[:len(elem.trucks[0].itinerary)]) + ",0\n")
            money = 0
            for stop in elem.trucks[0].itinerary:
                solutions_csv.write(str(money)+",")
                money = round(money+stop.money, 3)
            solutions_csv.write(str(money))
            solutions_csv.write("\n0," + ",".join(str(e+1) for e in elem.get_DNA()[len(elem.trucks[0].itinerary):len(elem.trucks[1].itinerary)+len(elem.trucks[0].itinerary)]) + ",0\n")
            money = 0
            for stop in elem.trucks[1].itinerary:
                solutions_csv.write(str(money) + ",")
                money = round(money+stop.money, 3)
            solutions_csv.write(str(money))
            solutions_csv.write("\n0," + ",".join(str(e+1) for e in elem.get_DNA()[len(elem.trucks[1].itinerary) + len(elem.trucks[0].itinerary):]) + ",0\n")
            money = 0
            for stop in elem.trucks[2].itinerary:
                solutions_csv.write(str(money) + ",")
                money = round(money+stop.money, 3)
            solutions_csv.write(str(money))
            solutions_csv.write("\n")


def serialize_run(run, front):
    """
    Serializes data of a run and the itineraries of chromosomes in the best front of that run
    in the "saved_data_file.txt" file of the "saved_data" directory.
    format :

        run : [run] of [date]
        mutRate : [MUT_RATE], it : [MAX_IT], pop size : [POP_SIZE]
        (dist, risk) : [itinerary 1],[itinerary 2],[itinerary 3]
        (dist, risk) : [itinerary 1],[itinerary 2],[itinerary 3]
             -> for each chromosome in the front 0 of the run

        ---------------------------------------------------

    :param date: date of run matching plot directory name
    :param run: number of the run of that date
    :param front: best front of the run
    """
    with open("saved_data/saved_data_file.txt", 'a') as saved_data_file:
        saved_data_file.write(
            "run : " + str(run) + '\n' + "mutRate : " + str(0.7) + ", it : " + str(
                10000) + ", pop size : " + str(300) + " mutDecr : log\n")
        for elem in front:
            saved_data_file.write(elem.__str__() + " : [" + ",".join(str(e) for e in elem.get_DNA()[:len(elem.trucks[0].itinerary)]) + "],["
                                  + ",".join(str(e) for e in elem.get_DNA()[len(elem.trucks[0].itinerary):len(elem.trucks[1].itinerary) + len(elem.trucks[0].itinerary)]) + "],["
                                  + ",".join(str(e) for e in elem.get_DNA()[len(elem.trucks[1].itinerary) + len(elem.trucks[0].itinerary):]) + "]\n")
        saved_data_file.write('\n---------------------------------------------------\n')


if __name__ == "__main__":
    print("Performing RANDOM")
    front0_random = rand_pop(300, max_time=5400)[0]
    serialize_run(100, front0_random)
