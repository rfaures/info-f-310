from Models.Stop import *

class Itinerary:

    def __init__(self, itinerary):
        self.itinerary = [Stop(id) for id in itinerary]
        self.distance = self.evaluate_distance()
        self.risk = self.evaluate_risk()

    def evaluate_distance(self):

        if self.itinerary != []:
            distance = Stop.distance_between(19, self.itinerary[0].id)
            for i in range(len(self.itinerary) - 1):
                distance += Stop.distance_between(self.itinerary[i].id, self.itinerary[i + 1].id)
            distance += Stop.distance_between(self.itinerary[-1].id, 19)
            return distance
        else:
            return 0

    def evaluate_risk(self):
        risk = 0
        if self.itinerary != []:
            money_collected = 0
            for i in range(len(self.itinerary) - 1):
                money_collected += self.itinerary[i].money
                risk += Stop.distance_between(self.itinerary[i].id, self.itinerary[i + 1].id) * money_collected
            money_collected += self.itinerary[-1].money
            risk += Stop.distance_between(self.itinerary[-1].id, 19) * money_collected
        return risk
