from Models.Itinerary import *


class Chromosome:

    def __init__(self, truck1, truck2, truck3):
        self.trucks = [Itinerary(truck1), Itinerary(truck2), Itinerary(truck3)]
        self.distance = 0
        self.risk = 0
        self.evaluate_distance()
        self.evaluate_risk()
        self.crowd_dist = 0
        self.front = 0


    def evaluate_distance(self):
        distance = 0
        for truck in self.trucks:
            distance += truck.distance
        if not self.check_constraints():
            self.distance = distance + 1000
        else:
            self.distance = distance

    def evaluate_risk(self):
        risk = 0
        for truck in self.trucks:
            risk += truck.risk
        if not self.check_constraints():
            self.risk = risk + 10000
        else:
            self.risk = risk

    def get_objective(self, o):
        if o == 0:
            return self.get_distance()
        elif o == 1:
            return self.get_risk()

    def get_DNA(self):
        DNA = []
        for truck in self.trucks:
            for stop in truck.itinerary:
                DNA.append(stop.id)
        return DNA

    def set_DNA(self, DNA):

        sizes = [len(self.trucks[0].itinerary), len(self.trucks[0].itinerary) + len(self.trucks[1].itinerary)]

        self.trucks = [Itinerary([id for id in DNA[:sizes[0]]]), Itinerary([id for id in DNA[sizes[0]:sizes[1]]]),
                       Itinerary([id for id in DNA[sizes[1]:]])]
        self.evaluate_risk()
        self.evaluate_distance()


    def get_risk(self):
        return self.risk

    def get_distance(self):
        return self.distance

    def get_itinerary(self):
        itinerary = []
        for truck in self.trucks:
            truck_iti = []
            for stop in truck.itinerary:
                truck_iti.append(stop.id)
            itinerary.append(truck_iti)
        return itinerary

    def check_constraints(self):

        res = True

        for i in range(3):
            big_district_count = 0
            money_count = 0
            for stop in self.trucks[i].itinerary:
                if stop.id in [0, 1, 9]:
                    big_district_count += 1
                money_count += stop.money
            if big_district_count == 3 or money_count > (839.1082 / 2):
                res = False
        return res

    def __str__(self):
        return "(" + "{:.2f}".format(self.get_distance()) + ", " + "{:.2f}".format(self.get_risk()) + ")"

    def __repr__(self):
        return self.__str__()

    def __eq__(self, other):
        return self.get_DNA() == other.get_DNA() and len(self.trucks[0].itinerary) == len(other.trucks[0].itinerary) \
               and len(self.trucks[1].itinerary) == len(other.trucks[1].itinerary)

    def __hash__(self):
        return hash((self.risk, self.distance))