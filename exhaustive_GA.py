from Models.Chromosome import Chromosome

from colour import Color
import matplotlib.pyplot as plt
import copy
import random
import numpy as np
from datetime import datetime
import ast
from pprint import pprint
from os import mkdir
from datetime import datetime
from NSGAII import non_dom_sort
from NSGAII import calculate_crowd_dist
from NSGAII import display_population
from NSGAII import compare_models
from NSGAII import compare_models_randomized

# put as attribute of class in constructor
# MAX_IT = 10
# POP_SIZE = 10
# mutation_rate = 0.01
# separator_1 = 6
# separator_2 = 12

repartitions = [[1, 9, 9], [5, 5, 9], [5, 6, 8], [5, 7, 7], [6, 6, 7]]



def crossover(parent_1, parent_2):
    parent_1_DNA = parent_1.get_DNA()
    parent_2_DNA = parent_2.get_DNA()

    child_1_DNA = []
    child_2_DNA = []

    i = random.randrange(19)
    j = random.randrange(18)

    start = min(i, j)
    end = max(i, j)

    child_1_DNA[start:end] = parent_1_DNA[start:end]
    child_2_DNA[start:end] = parent_2_DNA[start:end]

    index = 0
    id_1 = 0
    id_2 = 0

    for k in range(19):
        index = (end + k) % 19

        id_1 = parent_1_DNA[index]
        id_2 = parent_2_DNA[index]

        if id_2 not in child_1_DNA:
            child_1_DNA.append(id_2)

        if id_1 not in child_2_DNA:
            child_2_DNA.append(id_1)

    child_1_DNA = rotate(child_1_DNA, start)
    child_2_DNA = rotate(child_2_DNA, start)

    child_1 = exhaustive_random_chromosome()
    child_1.set_DNA(child_1_DNA)
    child_2 = exhaustive_random_chromosome()
    child_2.set_DNA(child_2_DNA)

    return child_1, child_2


def rotate(ls, n):
    return ls[-n:] + ls[:-n]


def compare(candidate_1, candidate_2):
    if candidate_1.front < candidate_2.front:
        return candidate_1
    elif candidate_1.front == candidate_2.front:
        if candidate_1.crowd_dist > candidate_1.crowd_dist:
            return candidate_1
        else:
            return candidate_2
    else:
        return candidate_2


def tournament_selection(pop):
    index_1 = random.randrange(len(pop))
    index_2 = random.randrange(len(pop))

    candidate_1 = pop[index_1]
    candidate_2 = pop[index_2]

    winner = compare(candidate_1, candidate_2)

    return winner


def produce_offspring(pop):
    children = []

    while len(children) < len(pop):
        parent_1 = tournament_selection(pop)
        parent_2 = tournament_selection(pop)
        parent_2_DNA = parent_2.get_DNA()

        parent_2 = tournament_selection(pop)
        parent_2_DNA = parent_2.get_DNA()

        child_1, child_2 = crossover(parent_1, parent_2)



        if MUTATION_FUNC == 1:
            mutation(child_1)
            mutation(child_2)
        elif MUTATION_FUNC == 2:
            mutation_2(child_1)
            mutation_2(child_2)

        children.append(child_1)
        children.append(child_2)

    return children


def mutation(individual):
    """
    Performs the mutation of an individual by swapping neighbours according to mutation_rate.
    This permutation allows for wrap-arounds. To exclude those cases, use
    index1 = randrange(len(DNA) - 1) ; index2 = (index1 + 1) instead.

    :param individual: the individual to mutate
    :param mutation_rate: 0.01 = 1 permutation, 1 = 100 permutations
    :return: the individual after mutation
    """

    DNA = individual.get_DNA()
    # print(DNA)

    for i in range(round(100 * MUT_RATE)):
        index1 = random.randrange(len(DNA))
        index2 = (index1 + 1) % len(DNA)
        DNA[index1], DNA[index2] = DNA[index2], DNA[index1]

    individual.set_DNA(DNA)

    return individual


def mutation_2(individual):
    """
    Performs the mutation of an individual by swapping neighbours according to mutation_rate.
    This permutation allows for wrap-arounds. To exclude those cases, use
    index1 = randrange(len(DNA) - 1) ; index2 = (index1 + 1) instead.

    :param individual: the individual to mutate
    :param mutation_rate: 0.01 = 1 permutation, 1 = 100 permutations
    :return: the individual after mutation
    """
    DNA = individual.get_DNA()
    # print(DNA)

    if random() < MUT_RATE:
        i = random.randrange(len(DNA))
        j = random.randrange(len(DNA))
        DNA[i], DNA[j] = DNA[j], DNA[i]

    individual.set_DNA(DNA)
    # print(individual.get_DNA())

    return individual

def exhaustive_random_chromosome():
    """
    Chromosome initialisation

    Returns a random chromosome.

    It first creates a random array of the 19 districts then splits it in three parts
    of defined length

    :return: random chromosome
    """
    districts = list(range(0, 19))
    itinerary = list()

    while len(districts) > 0:
        num = random.randrange(len(districts))
        itinerary.append(districts.pop(num))

    i = separator_1
    j = separator_2
    chromosome = Chromosome(itinerary[:min(i, j)], itinerary[min(i, j):max(i, j)], itinerary[max(i, j):])

    return chromosome


def exhaustive_GA():
    pop = []
    for i in range(POP_SIZE):
        pop.append(exhaustive_random_chromosome())
    # print(pop)

    j = 0
    stagnation = 0
    while j < MAX_IT and stagnation < MAX_STAGNATION:

        next_pop = []
        fronts = non_dom_sort(pop)
        calculate_crowd_dist(fronts)

        old_front = fronts[0]

        children = produce_offspring(pop)

        pop.extend(children)

        pop = list(set(pop))

        fronts = non_dom_sort(pop)
        calculate_crowd_dist(fronts)

        for front in fronts:
            if len(next_pop) + len(front) <= POP_SIZE:
                next_pop.extend(front)
            else:
                next_pop.extend(front[:POP_SIZE - len(next_pop)])
                break

        if set(fronts[0]) == set(old_front):
            stagnation += 1
        else:
            stagnation = 0

        print(stagnation)

        pop = next_pop
        j += 1
    print("it : ", j, "/", MAX_IT, " stagn :", stagnation, "/", MAX_STAGNATION)
    return fronts


def exhaustive_GA_progression():

    best_front_iterations = []
    pop = []
    for i in range(POP_SIZE):
        pop.append(exhaustive_random_chromosome())
    # print(pop)

    j = 0
    stagnation = 0
    while j < MAX_IT and stagnation < MAX_STAGNATION:

        next_pop = []
        fronts = non_dom_sort(pop)
        calculate_crowd_dist(fronts)

        old_front = fronts[0]

        children = produce_offspring(pop)

        pop.extend(children)

        pop = list(set(pop))

        fronts = non_dom_sort(pop)
        calculate_crowd_dist(fronts)

        for front in fronts:
            if len(next_pop) + len(front) <= POP_SIZE:
                next_pop.extend(front)
            else:
                next_pop.extend(front[:POP_SIZE - len(next_pop)])
                break

        if set(fronts[0]) == set(old_front):
            stagnation += 1
        else:
            print(stagnation)
            stagnation = 0
            fr = []
            for elem in fronts[0]:
                if elem.check_constraints():
                    fr.append(elem)
            best_front_iterations.append(fr)


        pop = next_pop
        j += 1
    print("it : ", j, "/", MAX_IT, " stagn :", stagnation, "/", MAX_STAGNATION)


    # print(fronts[0])
    #best_front_iterations.reverse()
    #display_population(best_front_iterations, 968)

    return best_front_iterations


def display_progression(best_front_iterations, n):
    plt.clf()
    plt.title("Population n°" + str(n))

    red = Color("red")
    colors = list(red.range_to(Color("green"), len(best_front_iterations)))


    index = 0
    for front in best_front_iterations:
        col= (round(colors[index].get_red(), 3), round(colors[index].get_green(), 3), round(colors[index].get_blue(), 3))
        if index == len(best_front_iterations)-1:
            col = 'black'
        print(col)
        for chrom in front:
            x = chrom.get_distance()
            y = chrom.get_risk()

            plt.scatter(x, y, c=col, s=10)

        index += 1

    plt.ylabel('Risque')
    plt.xlabel('Distance')

    plt.show()


def exhaustive_GA_iterator():
    global separator_1, separator_2

    all_best_fronts = []

    n = 0
    start = datetime.now()
    for repartition in repartitions:
        print("way :", n)

        separator_1 = repartition[0]
        separator_2 = separator_1 + repartition[1]
        print("sep 1 : ", separator_1, "sep 2: ", separator_2)

        fronts = exhaustive_GA()

        n += 1
        print(fronts[0])
        all_best_fronts.extend([elem for elem in fronts[0] if elem.check_constraints()])

    fronts = non_dom_sort(all_best_fronts)
    return fronts, (datetime.now() - start).seconds


def deserialize_run():
    """
    Creates a list of dictionaries for each saved run that contains the list of chromosomes in the best front of that
    run and data of the run.
    :func: `see <serialize_run> for serialization format`

    :return: the list of dictionnaries
    """

    chroms = []

    with open("saved_data/saved_data_file.txt", 'r') as save_file:
        content = save_file.readlines()

        for line in content:
            if line[0] == "r":
                chrom_run = []
                run_number, date = line.split(" of ")
                _, run_number = run_number.split(" : ")
                run_number = int(run_number)
                date = date[:-1]
            elif line[0] == "m":
                mut_rate, it, pop_size = line.split(", ");
                _, mut_rate = mut_rate.split(" : ");
                mut_rate = ast.literal_eval(mut_rate)
                _, it = it.split(" : ");
                it = ast.literal_eval(it)
                _, pop_size = pop_size.split(" : ");
                pop_size = ast.literal_eval(pop_size)
            elif line[0] == "(":
                _, gene = line.split(" : ")
                gene = ast.literal_eval(gene)
                chrom = Chromosome(gene[0], gene[1], gene[2])
                chrom_run.append(chrom)
            elif line[0] == "-":
                chroms.append(
                    {'chroms': chrom_run, "run_number": run_number, "date": date, "mut_rate": mut_rate, "it": it,
                     "pop_size": pop_size})
    return chroms


def serialize_run(date, run, fronts):
    """
    Serializes data of a run and the itineraries of chromosomes in the best front of that run
    in the "saved_data_file.txt" file of the "saved_data" directory.
    format :

        run : [run] of [date]
        mutRate : [MUT_RATE], it : [MAX_IT], pop size : [POP_SIZE]
        (dist, risk) : [itinerary 1],[itinerary 2],[itinerary 3]
        (dist, risk) : [itinerary 1],[itinerary 2],[itinerary 3]
             -> for each chromosome in the front 0 of the run

        ---------------------------------------------------

    :param date: date of run matching plot directory name
    :param run: number of the run of that date
    :param fronts: list of fronts resulting from the run
    """
    with open("saved_data/saved_data_file.txt", 'a') as saved_data_file:
        saved_data_file.write(
            "run : " + str(run) + " of " + date + '\n' + "mutRate : " + str(MUT_RATE) + ", it : " + str(
                MAX_IT) + ", pop size : " + str(POP_SIZE) + "\n")
        for elem in fronts[0]:
            saved_data_file.write(elem.__str__() + " : [" + ",".join(
                str(e) for e in elem.get_DNA()[:len(elem.trucks[0].itinerary)]) + "],[" + ",".join(str(e) for e in
                                                                                                   elem.get_DNA()[
                                                                                                   len(elem.trucks[
                                                                                                           0].itinerary):len(
                                                                                                       elem.trucks[
                                                                                                           1].itinerary) + len(elem.trucks[0].itinerary)]) + "],["
                                  + ",".join(
                str(e) for e in elem.get_DNA()[len(elem.trucks[1].itinerary) + len(elem.trucks[0].itinerary):]) + "]\n")
        saved_data_file.write('\n---------------------------------------------------\n')



def check_best_repartitions(top_front_number):
    """
    Counts the occurences of repartitions in the best [top_front_number] fronts of the non_dom_sorting of
    serialized best fronts. Allows to make hypothesis to discard inefficient repartitions.

    :param top_front_number: the number of front from which repartiton occurences are counted.

    """
    chrom_dics = deserialize_run()

    winner_ways = []
    pop = []
    for chrom_dic in chrom_dics:
        #if len(pop) > 2250:
        #    break
        chroms = chrom_dic.get("chroms")
        pop.extend(chroms)
        for chrom in chroms:
            way = [len(chrom.trucks[0].itinerary), len(chrom.trucks[1].itinerary), len(chrom.trucks[2].itinerary)]

            winner_ways.append(way)
        # print(chrom_dic.get("pop_size"))
        # pop.extend(chroms)

    reps = []
    cnts = []
    for repartition in repartitions:
        reps.append(repartition)
        cnt = winner_ways.count(repartition)
        cnts.append(cnt)
        #print("winner : ", repartition, " count : ", cnt)

    for i in range(len(reps)):
        print(reps[i], cnts[i])


    best_winners_ways = []
    fronts = non_dom_sort(pop)
    for f in range(top_front_number):
        for chrom in fronts[f]:
            way = [len(chrom.trucks[0].itinerary), len(chrom.trucks[1].itinerary), len(chrom.trucks[2].itinerary)]

            best_winners_ways.append(way)

    top_counts = []
    for repartition in repartitions:
        top_cnt = best_winners_ways.count(repartition)
        top_counts.append(top_cnt)
        print("best winner :", repartition, " count : ", top_cnt)

    return reps, top_counts

def check_best_mut_rate(top_front_number):
    chrom_dics = deserialize_run()

    pop = []
    muts = []
    for chrom_dic in chrom_dics:

        chroms = chrom_dic.get("chroms")
        mut = round(chrom_dic.get("mut_rate"),2)
        pop.extend(chroms)
        for chrom in chroms:
            chrom.mut = mut
            muts.append(mut)

    fronts = non_dom_sort(pop)

    best_mut = []
    for f in range(top_front_number):
        for chrom in fronts[f]:
            best_mut.append(chrom.mut)

    for m in range(20):
        dm = round(m/100,2)
        print("best winner :", dm, "count ratio: ", best_mut.count(dm) / muts.count(dm))

def check_best_population(top_front_number):
    chrom_dics = deserialize_run()

    pop = []
    muts = []
    for chrom_dic in chrom_dics:

        chroms = chrom_dic.get("chroms")
        mut = chrom_dic.get("pop_size")
        pop.extend(chroms)
        for chrom in chroms:
            chrom.mut = mut
            muts.append(mut)

    fronts = non_dom_sort(pop)

    best_mut = []
    for f in range(top_front_number):
        for chrom in fronts[f]:
            best_mut.append(chrom.mut)

    for m in range(25):
        dm = 5 * (m + 1)
        print("best winner :", dm, "count ratio: ", best_mut.count(dm)/ muts.count(dm))



def bar_graph_repartitions(top_front_number):

    reps, cnts = check_best_repartitions(top_front_number)

    reps = tuple(reps)
    x_pos = np.arange(len(reps))
    plt.bar(x_pos, cnts, align='center', alpha=0.5)
    plt.xticks(x_pos, reps)
    plt.show()


if __name__ == "__main__":


    chrom_dicts = deserialize_run()

    pop = []
    for chrom_dic in chrom_dicts:
        chroms = chrom_dic.get("chroms")
        pop.extend(chroms)


    pop = list(set(pop))

    fronts = non_dom_sort(pop)

    display_population([fronts[0]], 888)



    """
    global separator_1, separator_2
    global MUT_RATE, POP_SIZE, MAX_IT, MAX_STAGNATION, MUATION_FUNC

    MUT_RATE = 0.01
    MAX_IT = 5000
    MAX_STAGNATION = 300
    POP_SIZE = 500
    MUTATION_FUNC = 1


    
    date = (str(datetime.now()).replace(" ", "_")).replace(":", "'")
    mkdir("plots/" + date)
    for run in range(100):
        print("run number :", run)
        all_best_fronts = []
        n = 0
        for repartition in repartitions:
            #print("way :", n)

            separator_1 = repartition[0]
            separator_2 = separator_1 + repartition[1]
            #print("sep 1 : ", separator_1, "sep 2: ", separator_2)

            fronts = exhaustive_GA()

            n += 1
            #print(fronts[0])
            for elem in fronts[0]:
                if elem.check_constraints():
                    all_best_fronts.append(elem)

        fronts = non_dom_sort(all_best_fronts)
        display_population(fronts, run, date)

        serialize_run(date, run, fronts)
    """



