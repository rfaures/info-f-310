from NSGAII import non_dom_sort


def compare(models_front0):
    all_chromosomes = []
    for model in models_front0:
        all_chromosomes += model

    new_fronts = non_dom_sort(all_chromosomes)

    models_perf = []

    for model in models_front0:
        model_perf = []
        for front in new_fronts:
            i = 0
            for chromosome in model:
                if chromosome in front:
                    i+=1
            model_perf.append(i/len(model))
        models_perf.append(model_perf)

    print(models_perf)